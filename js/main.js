class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, car_id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._car_id = car_id;
    }
    move(xPosition) {
        this._xPos -= xPosition;
        this._element.classList.add('flying');
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this._explosion = document.getElementById('explosion');
        this._loop = true;
        this.loop = () => {
            if (this._loop) {
                this.collision();
                this.update();
                requestAnimationFrame(this.loop);
                console.log("Loop");
            }
        };
        this.keyDownHandler = (e) => {
            console.log(e);
            if (e.keyCode === 32) {
                console.log("Keydown");
                this._car.move(50);
                this.update();
            }
            else {
                console.log("nog niet");
            }
        };
        this._car = new Car('car', 0);
        this._parking = new Parking('spot', 0);
        this._scoreboard = new Scoreboard('scoreboard');
        this._success = new Rating('up', 0);
        this._fail = new Rating('down', 0);
        this._explosion.style.display = 'none';
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
        this.loop();
    }
    collision() {
        const parkingRect = document.getElementById('spot-0').getBoundingClientRect();
        const carRect = document.getElementById('car').getBoundingClientRect();
        let distance = parkingRect.left - carRect.left;
        if (distance < 0 && distance > -25) {
            this._loop = false;
            this._scoreboard.addScore();
        }
        this._loop = true;
        if (distance >= 0) {
            this._scoreboard.removeScore();
            this._explosion.style.display = 'block';
            setTimeout(function () {
                const explosiontest = document.getElementById('explosion');
                explosiontest.style.display = 'none';
            }, 1700);
            this._fail.draw;
            this._loop = false;
            console.log("Boooom");
        }
        if (distance < -150 && distance > -500) {
            window.removeEventListener('keydown', this.keyDownHandler);
        }
    }
    draw() {
        this._car.draw(this._element);
        this._parking.draw(this._element);
        this._scoreboard.draw(this._element);
    }
    update() {
        this._car.update();
        this._parking.update();
        this.collision();
        this._scoreboard.update();
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class Parking extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
}
class Rating extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    get score() {
        return this._score;
    }
    addScore() {
        this._score += 1;
    }
    removeScore() {
        this._score -= 1;
    }
}
//# sourceMappingURL=main.js.map