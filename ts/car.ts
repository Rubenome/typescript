/// <reference path="gameItem.ts" />

class Car extends GameItem {

        private _car_id:number;    

        /**
        * Function to create the Character
        * @param {string} - name
        * @param {car_id} - id
        * @param {number} - xPosition
        * @param {number} - yPosition
        */
        constructor(name: string, car_id: number, xPosition: number = 0, yPosition: number = 0) {
            super(name, xPosition, yPosition);
            this._car_id = car_id;
        }
    
        /**
        * Function to move the car left
        * @param {number} - xPosition
        */
        public move(xPosition: number): void {
            this._xPos -= xPosition;
            this._element.classList.add('flying');
        }
    }