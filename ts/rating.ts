/// <reference path="gameItem.ts" />

class Rating extends GameItem {
    private _id: number;

    /**
    * Function to create the GameItem
    * @param {string} - name
    * @param {number} - id
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, id: number, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
}