class Game {
  //attr
  private _element: HTMLElement = document.getElementById('container');
  private _explosion: HTMLElement = document.getElementById('explosion');
  private _car: Car;
  private _parking: Parking;
  private _scoreboard: Scoreboard;
  private _success: Rating;
  private _fail: Rating;
  private _loop: boolean = true;

  /**
   * Create the Game class
   */
  constructor() {
    this._car = new Car('car', 0);
    this._parking = new Parking('spot', 0);
    this._scoreboard = new Scoreboard('scoreboard');
    this._success = new Rating('up', 0);
    this._fail = new Rating('down', 0);
    this._explosion.style.display = 'none';

    //add keydown handler to the window object
    window.addEventListener('keydown', this.keyDownHandler);

    //draw is initial state
    this.draw();
    this.loop();
  }

  /**
   * Loop function to detect position
   */
  private loop = () => {
    if (this._loop) {
      this.collision();
      this.update();
      requestAnimationFrame(this.loop);
      console.log("Loop");
    }
  }

  /**
   * Function to detect collision between two objects
   */
  public collision(): void {
    const parkingRect = document.getElementById('spot-0').getBoundingClientRect();
    const carRect = document.getElementById('car').getBoundingClientRect();
    let distance = parkingRect.left - carRect.left;

    //Detect if car is on parking spot
    if (distance < 0 && distance > -25) {
      this._loop = false;
      this._scoreboard.addScore();
    }
    this._loop = true;

    //Detect if car is beyond parking spot
    if (distance >= 0) {
      this._scoreboard.removeScore();
      this._explosion.style.display = 'block';
      setTimeout(function () {
        const explosiontest = document.getElementById('explosion');
        explosiontest.style.display = 'none';
      }, 1700);
      this._fail.draw;
      this._loop = false;
      console.log("Boooom");
    }

    // When to stop the keyDownHandler
    if (distance < -150 && distance > -500) {
      window.removeEventListener('keydown', this.keyDownHandler);
    }
  }

  /** 
   * Function to draw the initial state of al living objects
   */
  public draw(): void {
    this._car.draw(this._element);
    this._parking.draw(this._element);
    this._scoreboard.draw(this._element);
  }

  /**
   * Function to update the state of all living objects
   */
  public update(): void {
    this._car.update();
    this._parking.update();
    this.collision();
    this._scoreboard.update();
  }

  /**
   * Function to handle the keyboard event
   * @param {KeyboardEvent} - event
   */
  public keyDownHandler = (e: KeyboardEvent): void => {
    console.log(e);
    if (e.keyCode === 32) {
      console.log("Keydown");
      this._car.move(50);
      this.update();
    }
    else {
      console.log("nog niet");
    }
  }
}